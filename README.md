# Drools

Docker4drupal Bash script shortcuts for Windows/MAC/linux to make developers life easier

Offers quick access commands to drupal most used features :

	- clear cache
	- import/export config
	- export a database dump
	- enter a specific container
	- composer commands shortcuts
	

![alt text](https://gitlab.com/grumpyDev74/drools/-/raw/master/drools.png)